import UIKit

// Advent of Code 2019 Day 2
// https://adventofcode.com/2019/day/2

enum Oppcode: Int {
    case add = 1
    case multiply = 2
    case complete = 99
}

enum OppcodeData {
    case add(OppcodePositions)
    case multiply(OppcodePositions)
    case complete
}

struct OppcodePositions {
    let value1Location: Int
    let value2Location: Int
    let resultLocation: Int
}

enum OppcodeError: Error {
    case badOppcode
}

let opcodeLength = 4

let input = [1,0,0,3,
             1,1,2,3,
             1,3,4,3,
             1,5,0,3,
             2,1,13,19,
             1,10,19,23,
             2,9,23,27,
             1,6,27,31,
             1,10,31,35,
             1,35,10,39,
             1,9,39,43,
             1,6,43,47,
             1,10,47,51,
             1,6,51,55,
             2,13,55,59,
             1,6,59,63,
             1,10,63,67,
             2,67,9,71,
             1,71,5,75,
             1,13,75,79,
             2,79,13,83,
             1,83,9,87,
             2,10,87,91,
             2,91,6,95,
             2,13,95,99,
             1,10,99,103,
             2,9,103,107,
             1,107,5,111,
             2,9,111,115,
             1,5,115,119,
             1,9,119,123,
             2,123,6,127,
             1,5,127,131,
             1,10,131,135,
             1,135,6,139,
             1,139,5,143,
             1,143,9,147,
             1,5,147,151,
             1,151,13,155,
             1,5,155,159,
             1,2,159,163,
             1,163,6,0,
             99,2,0,14,0]


class Computer {
    var intArray: [Int]

    init(intArray: [Int]) {
        self.intArray = intArray
    }

    func getOutput() -> Int {
        return intArray[0]
    }

    var noun: Int {
        get {
            return intArray[1]
        }
        set (value){
            intArray[1] = value
        }
    }

    var verb: Int {
        get {
            return intArray[2]
        }
        set (value){
            intArray[2] = value
        }
    }

    func runProgram() {
        var position = 0
        repeat {
            // get next command
            let oppcodeDataResult = buildOppcodeData(from: intArray, position: position)
            switch oppcodeDataResult {
            case .success(let oppcodeData):
                let isComplete = process(oppcodeData: oppcodeData)

                if isComplete {
                    return
                }

                position += opcodeLength

            case .failure(let error):
                print("error: \(error)")
                return
            }
        } while position < intArray.count
    }

    func buildOppcodePositions(from intArray: [Int], position: Int)-> OppcodePositions {
        // no sanity check or check for out of bounds

        let value1Location = intArray[position+1]
        let value2Location = intArray[position+2]
        let resultLocation = intArray[position+3]

        return OppcodePositions(
            value1Location: value1Location,
            value2Location: value2Location,
            resultLocation: resultLocation
        )
    }

    func buildOppcodeData(from intArray: [Int], position: Int) -> Result<OppcodeData, Error> {
        // check for a valid oppcode
        guard let oppCode = Oppcode(rawValue: intArray[position]) else {
            return .failure(OppcodeError.badOppcode)
        }

        if oppCode == .complete {
            return .success(.complete)
        }

        switch oppCode {
        case .complete:
            return .success(.complete)
        case .add:
            let oppcodePositions = buildOppcodePositions(from: intArray, position: position)
            return .success(.add(oppcodePositions))
        case .multiply:
            let oppcodePositions = buildOppcodePositions(from: intArray, position: position)
            return .success(.multiply(oppcodePositions))
        }
    }

    func process(oppcodeData: OppcodeData) -> Bool {
        switch oppcodeData {
        case .add(let oppcodePositions):
            processAdd(oppcodePositions: oppcodePositions)
            return false
        case .multiply(let oppcodePositions):
            processMultiply(oppcodePositions: oppcodePositions)
            return false
        case .complete:
            return true
        }
    }

    func processAdd(oppcodePositions: OppcodePositions) {
        let value1 = intArray[oppcodePositions.value1Location]
        let value2 = intArray[oppcodePositions.value2Location]

        intArray[oppcodePositions.resultLocation] = value1 + value2
    }

    func processMultiply(oppcodePositions: OppcodePositions) {
        let value1 = intArray[oppcodePositions.value1Location]
        let value2 = intArray[oppcodePositions.value2Location]

        intArray[oppcodePositions.resultLocation] = value1 * value2
    }
}

var workingData = input

// To do this, before running the program, replace position 1 with the value 12 and replace position 2 with the value 2.

workingData[1] = 12
workingData[2] = 2

let computer = Computer(intArray: workingData)

computer.runProgram()

print("value at position 0 \(computer.intArray[0])")



// Part 2

func createCleanInput() -> [Int] {
    var workingData = input

    workingData[1] = 12 // noun
    workingData[2] = 2 // verb

    return workingData
}

func createComputer(noun: Int, verb: Int) -> Computer {
    let computer = Computer(intArray: input)
    computer.noun = noun
    computer.verb = verb
    return computer
}

let requiredAnswer = 19690720

let possibleNounValues = 0...99
let possibleVerbValues = 0...99

var answer: Int?
// Brute force
for nounValue in possibleNounValues {
    for verbValue in possibleVerbValues {
        let computer = createComputer(noun: nounValue, verb: verbValue)
        computer.runProgram()
        let output = computer.getOutput()

        if output == requiredAnswer {
            answer = 100 * nounValue + verbValue
            break
        }
    }

    if answer != nil {
        break
    }
}

if let answer = answer {
    print("The answer is \(answer)")
} else {
    print("Not found")
}
